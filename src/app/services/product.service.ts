import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private db : AngularFireDatabase) { }

  create(product){
    return this.db.list('/products').push(product);
  }

  getAll(){
    return this.db.list('/products').snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() as Product }))
      )
    );
  }

  get(id){
    return this.db.object('/products/' + id).valueChanges();
  }

  update(productid,product){
    this.db.object('/products/'+productid).update(product);
  }

  delete(productid){
    this.db.object('/products/'+productid).remove();
  }
}
