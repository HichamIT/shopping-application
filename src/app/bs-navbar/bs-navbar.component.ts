import { User } from './../models/user';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { auth } from 'firebase';


@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent {

  appUser : User;

  constructor(private authService : AuthService) { 
    authService.appUser$.subscribe(appuser => this.appUser = appuser);
  }

  Logout(){
    this.authService.logout();
  }

}
