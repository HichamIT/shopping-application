import { AdminOrdersComponent } from './admin/admin-orders/admin-orders.component';
import { AdminProductsComponent } from './admin/admin-products/admin-products.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { OrdersSuccessComponent } from './orders-success/orders-success.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './core/auth.guard';
import { AdminGuard } from './core/admin.guard';
import { ProductFormComponent } from './admin/product-form/product-form.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'shopping-cart', component: ShoppingCartComponent },
  { path: 'login', component: LoginComponent },

  { path: 'check-out', component: CheckOutComponent , canActivate: [AuthGuard] },
  { path: 'orders-success', component: OrdersSuccessComponent , canActivate: [AuthGuard] },
  { path: 'my/orders', component: MyOrdersComponent , canActivate: [AuthGuard]},
  
  { path: 'admin/product/new', component: ProductFormComponent , canActivate: [AuthGuard,AdminGuard] },
  { path: 'admin/product/:id', component: ProductFormComponent , canActivate: [AuthGuard,AdminGuard] },
  { path: 'admin/admin-products', component: AdminProductsComponent , canActivate: [AuthGuard,AdminGuard] },
  
  { path: 'admin/admin-orders', component: AdminOrdersComponent , canActivate: [AuthGuard,AdminGuard] },
  
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
