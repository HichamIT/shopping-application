import { Product } from './../../models/product';
import { ProductService } from './../../services/product.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { map, filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit,OnDestroy {
  
  
  product : Product[];
  filtredProduct : any[];
  subscription : Subscription;

  constructor(private productservice : ProductService) { 
      this.subscription = this.productservice.getAll().subscribe(data => this.filtredProduct = this.product = data); 
  }

  filter(query : string){
    this.filtredProduct = (query) ? 
        this.product.filter(p => p.title.toLocaleLowerCase().includes(query.toLocaleLowerCase())) : this.product;
    
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit() {
  }

}
