import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from './../../services/product.service';
import { GategoryService } from './../../services/gategory.service';
import { Component, OnInit } from '@angular/core';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  categories : any[]
  product : {}
  id

  constructor(categoryservice : GategoryService , 
    private productservice : ProductService,
    private route : Router,
    private router : ActivatedRoute) { 

    categoryservice.getCategory().subscribe(data => this.categories= data);
    this.id = this.router.snapshot.paramMap.get('id');
    if(this.id)
      this.productservice.get(this.id).subscribe(data => this.product= data); 
    }

  save(product){
    if(this.id)
      this.productservice.update(this.id,product);
    else
       this.productservice.create(product);
    this.route.navigate(['/admin/admin-products'])
  }

  delete(){
    if (!confirm('Are you Sure to delete this Products ')) return;
    this.productservice.delete(this.id)
    this.route.navigate(['/admin/admin-products'])
  }
  ngOnInit() {
  }

}
