import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  constructor(private user : UserService ,private afAuth : AuthService , route : Router){
    afAuth.user$.subscribe(user => {
      if(!user) return 
        this.user.save(user);
        let returnUrl = localStorage.getItem('returnUrl');
      
      if (!returnUrl) return
        localStorage.removeItem(returnUrl);
        route.navigateByUrl(returnUrl);
    
      
    })
  }
}
